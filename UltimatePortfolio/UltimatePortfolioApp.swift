//
//  UltimatePortfolioApp.swift
//  UltimatePortfolio
//
//  Created by Gareth Fleming on 17/02/2023.
//

import SwiftUI

@main
struct UltimatePortfolioApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
